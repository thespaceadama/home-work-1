// task 2

function printArrayValues(arr) {
   if (arr.length===0) return;
    for (let i = 0; i < arr.length; i++) {
        console.log(arr[i], 'array');
    }
    printArrayValues([])
}
printArrayValues([1,2,3])
// task 3

let arr1 = [4, 5, 6];
let adam = [1, 2, 3, ...arr1, 7, 8, 9, 10];
console.log(adam);


// task 4

let arr2 = [1, 2, 3, 4, 5];
let lastArr = arr2[arr2.length - 1]
console.log(lastArr)

// task 5

const countChar = (str, sym) =>{
    let x = 0;
    for (let i = 0; i<=str.length; i++){
        if (str.charAt(i)===sym){
            x++;
        }
    }
    console.log(x)
}

countChar('loremipsumdolor', 'o')
